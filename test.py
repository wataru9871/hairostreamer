import numpy as np
import cv2
import threading
caps = []
caps.append(cv2.VideoCapture("http://192.168.2.101:8080/?action=stream latency=0"))
caps.append(cv2.VideoCapture("http://192.168.2.101:8081/?action=stream latency=0"))
caps.append(cv2.VideoCapture("http://192.168.2.101:8082/?action=stream latency=0"))
caps.append(cv2.VideoCapture("http://192.168.2.101:8083/?action=stream latency=0"))

height = 240*2
width = 320*2

def getCam(num, arg_frames):
	while True:
		ret, frame = caps[num].read()
		if ret is True:
			arg_frames[(num//2)*240:(num//2)*240+240,((num%2)*320):(320+(num%2)*320)] = frame

threadlist = [None,None,None,None]
frames = np.zeros((height,width,3),np.uint8)

for thread_num in range(0, 4):
	thread = threading.Thread(target=getCam, args=([thread_num, frames]))
	threadlist[thread_num] = thread

for thread in threadlist:
	if not thread is None:
		thread.start()
while True:
	frame = cv2.resize(frames,None,fx =1.5,fy=1.5)
	cv2.imshow("cam", frame)

	if cv2.waitKey(1) & 0xFF == ord('q') :
		break

for cap in caps:
	cap.release()

cv2.destroyAllWindows()

	

